#Add imports of modules here
import utility
import datetime

#Constants
FILENAME_IN = "spy.in.csv"
FILENAME_OUT = "spy_dollar_cost_average.out.csv"
CASH = 100000.00
START_DATE = datetime.datetime(1951, 1, 1)
END_DATE = datetime.datetime(2100, 1, 1)

#Lets read the input file, and put it in a Python List
lst_daily_data = utility.read_csv_input(FILENAME_IN, START_DATE, END_DATE)

###########################################################

#Initialize for overall daily processing
flt_cash = CASH
int_shares = 0
lst_yearly_returns = []

#Keep track of the yearly starting values
flt_account_value_start_year = flt_cash
int_days_in_year = 0
lst_trades_in_year = []

#Algorithm specific variables here
flt_buy_pct = 0.01 #buy this much of your savings
flt_buy_pct_dip = 0.20 #buy this much of your savings
flt_down_trend_pct = -0.10 #this tells how much loss is considered a down trend
days_since_last_purchase = 0
days_since_last_purchase_max = 20 #this is trading days, so about 20 per month
flt_last_purchase_price = 999999999 #any large number will do, that is larger than our prices


#Lets process each row, one day at a time
for indexToday in range(0, len(lst_daily_data)):
    row_day = lst_daily_data[indexToday]
    row_yesterday = lst_daily_data[indexToday - 1]
    row_day_tx_price = row_day['CLOSE_PRICE']

    ################### ALGORITHM - START #####################

    #Determine the trade for today
    #Buy periodically, lets say the first trading day of the month
    #Buy more when we are on the way down, relative to our most recent high

    #Check if its time to buy
    flt_trend_pct = (row_day_tx_price - flt_last_purchase_price) / flt_last_purchase_price
    if (flt_trend_pct <= flt_down_trend_pct):
        flt_amount_to_buy = flt_cash * flt_buy_pct_dip
        (int_shares_bought, flt_cash_left) = utility.buy(flt_amount_to_buy, row_day['CLOSE_PRICE'])
        flt_cash = flt_cash - flt_amount_to_buy + flt_cash_left
        int_shares = int_shares + int_shares_bought
        flt_last_purchase_price = row_day['CLOSE_PRICE']
        days_since_last_purchase = 0
        the_trade = utility.log_tx('BUY_DIP', int_shares_bought, row_day)
        lst_trades_in_year.append(the_trade)
    elif (days_since_last_purchase >= days_since_last_purchase_max):
        flt_amount_to_buy = flt_cash * flt_buy_pct
        (int_shares_bought, flt_cash_left) = utility.buy(flt_amount_to_buy, row_day['CLOSE_PRICE'])
        flt_cash = flt_cash - flt_amount_to_buy + flt_cash_left
        int_shares = int_shares + int_shares_bought
        flt_last_purchase_price = row_day['CLOSE_PRICE']
        days_since_last_purchase = 0
        the_trade = utility.log_tx('BUY', int_shares_bought, row_day)
        lst_trades_in_year.append(the_trade)
    else:
        #Dont trade today, just sit tight
        days_since_last_purchase = days_since_last_purchase + 1

    ################### ALGORITHM - END #######################

    #The trading day is over
    (flt_account_value_start_year, int_days_in_year, lst_trades_in_year) = utility.trading_day_closed(lst_yearly_returns,
        lst_daily_data, indexToday,
        int_shares, flt_cash, None,
        flt_account_value_start_year, int_days_in_year, lst_trades_in_year)

###########################################################

#Lets save the results
utility.write_csv_output(FILENAME_OUT, lst_yearly_returns)
