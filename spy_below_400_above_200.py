#Add imports of modules here
import utility
import datetime

#Constants
FILENAME_IN = "spy.in.csv"
FILENAME_OUT = "spy_below_400_above_200.out.csv"
CASH = 10000.00
START_DATE = datetime.datetime(1951, 1, 1)
END_DATE = datetime.datetime(2100, 1, 1)

#Lets read the input file, and put it in a Python List
lst_daily_data = utility.read_csv_input(FILENAME_IN, START_DATE, END_DATE)

###########################################################

#Initialize for overall daily processing
flt_cash = CASH
int_shares = 0
lst_yearly_returns = []

#Keep track of the yearly starting values
flt_account_value_start_year = flt_cash
int_days_in_year = 0
lst_trades_in_year = []

#Algorithm specific variables here
vested = False

#Lets process each row, one day at a time
for indexToday in range(0, len(lst_daily_data)):
    row_day = lst_daily_data[indexToday]
    row_yesterday = lst_daily_data[indexToday-1]

    ################### ALGORITHM - START #####################

    #Determine the trade for today
    #When the price is above 200SMA or 400SMA, we are always in
    #When the price is goes below 400SMA, when the 400SMA is below the 200SMA, we get out

    if (not vested):
        if ((row_day['CLOSE_PRICE'] >= row_day['SMA200']) or (row_day['CLOSE_PRICE'] >= row_day['SMA400'])):
            (int_shares_bought, flt_cash_left) = utility.buy(flt_cash, row_day['CLOSE_PRICE'])
            int_shares = int_shares + int_shares_bought
            flt_cash = flt_cash_left
            vested = True
            the_trade = utility.log_tx('BUY', int_shares_bought, row_day)
            lst_trades_in_year.append(the_trade)
        else:
            #Dont buy today
            pass
    else: #vested=True
        if (row_day['CLOSE_PRICE'] < row_day['SMA400']) and (row_day['SMA400'] < row_day['SMA200']):
            if (row_yesterday['CLOSE_PRICE'] >= row_day['SMA400']):
                flt_cash_profit = utility.sell(int_shares, row_day['CLOSE_PRICE'])
                int_shares_sold = int_shares
                int_shares = 0
                flt_cash = flt_cash + flt_cash_profit
                vested = False
                the_trade = utility.log_tx('SELL', int_shares_sold, row_day)
                lst_trades_in_year.append(the_trade)
        else:
            #Dont sell today
            pass

    ################### ALGORITHM - END #######################

    #The trading day is over
    (flt_account_value_start_year, int_days_in_year, lst_trades_in_year) = utility.trading_day_closed(lst_yearly_returns,
        lst_daily_data, indexToday,
        int_shares, flt_cash, None,
        flt_account_value_start_year, int_days_in_year, lst_trades_in_year)

###########################################################

#Lets save the results
utility.write_csv_output(FILENAME_OUT, lst_yearly_returns)
