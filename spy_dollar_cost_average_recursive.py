#Add imports of modules here
import utility
import datetime

#Constants
FILENAME_IN = "spy.in.close_only.csv"

# Timeframe to load
START_YEAR = 1950
END_YEAR = 2020

# Constants - Algorithm specific variables
BUY_AMOUNT_PCT = 1  # buy this much of your savings
BUY_AMOUNT_PCT_DIP = 20  # buy this much of your savings
DOWN_TREND_PCT = 5  # this tells how much loss is considered a down trend
BUY_FREQUENCY_MIN = 20  # this is trading days, so about 20 == a month
TIME_PERIOD_YEARS = 3

# Lets read the input file, and put it in a Python List
START_DATE = datetime.datetime(START_YEAR, 1, 1)
END_DATE = datetime.datetime(END_YEAR, 12, 31)
lst_daily_data = utility.read_csv_input(FILENAME_IN, START_DATE, END_DATE)

# Result
lst_periodic_returns = []

for current_start_year in range(START_YEAR, END_YEAR):
    # Base values used across time periods
    START_CASH = 100000.00

    ###########################################################

    # Runtime - Algorithm specific variables
    flt_buy_pct = 1.0 * BUY_AMOUNT_PCT / 100
    flt_buy_pct_dip = 1.0 * BUY_AMOUNT_PCT_DIP / 100
    flt_down_trend_pct = -1.0 * DOWN_TREND_PCT / 100
    days_since_last_purchase_trigger = BUY_FREQUENCY_MIN
    int_years_to_aggregate = TIME_PERIOD_YEARS

    # Initialize for overall daily processing
    flt_cash = START_CASH
    int_shares = 0
    lst_trades_in_time_period = []
    flt_last_purchase_price = 1  # any non-zero number will do, to get the system started
    int_buy_dips_count = 0
    day_in_time_period = 0
    days_since_last_purchase = 0
    current_end_year = current_start_year + int_years_to_aggregate - 1

    #Lets process each row, one day at a time
    for indexToday in range(0, len(lst_daily_data)):
        row_day = lst_daily_data[indexToday]
        row_yesterday = lst_daily_data[indexToday - 1]
        row_day_tx_price = row_day['CLOSE_PRICE']

        #Make sure we are in the time period range
        if (row_day['YEAR'] < current_start_year):
            # we have not yet reached out time period, go the next day
            continue
        elif (row_day['YEAR'] >= current_start_year and row_day['YEAR'] <= current_end_year):
            # this row_day is part of our time period
            day_in_time_period = day_in_time_period + 1
            pass
        else:
            # done, we have processed all the days necessary to for this time period
            flt_account_value_end = utility.total_account(int_shares, row_day_tx_price, flt_cash)
            utility.aggregate_time_period(lst_periodic_returns, lst_daily_data, current_start_year, current_end_year, START_CASH, flt_account_value_end, flt_cash, lst_trades_in_time_period, int_buy_dips_count)
            break

        ################### ALGORITHM - START #####################

        #Determine the trade for today
        #Buy periodically, lets say the first trading day of the month
        #Buy more when we are on the way down, relative to our most recent high

        #Check if its time to buy
        flt_trend_pct = (row_day_tx_price - flt_last_purchase_price) / flt_last_purchase_price
        if (flt_trend_pct <= flt_down_trend_pct):
            flt_amount_to_buy = flt_cash * flt_buy_pct_dip
            (int_shares_bought, flt_cash_left) = utility.buy(flt_amount_to_buy, row_day_tx_price)
            flt_cash = flt_cash - flt_amount_to_buy + flt_cash_left
            int_shares = int_shares + int_shares_bought
            flt_last_purchase_price = row_day_tx_price
            days_since_last_purchase = 0
            int_buy_dips_count = int_buy_dips_count + 1
            the_trade = utility.log_tx('BUY_DIP', int_shares_bought, row_day)
            lst_trades_in_time_period.append(the_trade)
        elif (day_in_time_period == 1 or days_since_last_purchase >= days_since_last_purchase_trigger):
            flt_amount_to_buy = flt_cash * flt_buy_pct
            (int_shares_bought, flt_cash_left) = utility.buy(flt_amount_to_buy, row_day_tx_price)
            flt_cash = flt_cash - flt_amount_to_buy + flt_cash_left
            int_shares = int_shares + int_shares_bought
            flt_last_purchase_price = row_day_tx_price
            days_since_last_purchase = 0
            the_trade = utility.log_tx('BUY', int_shares_bought, row_day)
            lst_trades_in_time_period.append(the_trade)
        else:
            #Dont trade today, just sit tight
            days_since_last_purchase = days_since_last_purchase + 1

        ################### ALGORITHM - END #######################

        #The trading day is over
        pass

    ###########################################################

    #Done with inner for-loop
    print("done with START_YEAR - %s" % (current_start_year))

#All done
FILENAME_OUT = "spy_dca_%syears_%sdays_%strend_%sbuy.out.csv" % (TIME_PERIOD_YEARS, BUY_FREQUENCY_MIN, DOWN_TREND_PCT, BUY_AMOUNT_PCT_DIP)
utility.write_csv_output_for_aggregate_time_period(FILENAME_OUT, lst_periodic_returns)