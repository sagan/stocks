#Add imports of modules here
import utility
import datetime

#Constants
FILENAME_IN = "spy.in.csv"
FILENAME_OUT = "spy_trailing_min_max.out.csv"
CASH = 10000.00
START_DATE = datetime.datetime(1951, 1, 1)
END_DATE = datetime.datetime(2100, 1, 1)

#Lets read the input file, and put it in a Python List
lst_daily_data = utility.read_csv_input(FILENAME_IN, START_DATE, END_DATE)

###########################################################

#Initialize for overall daily processing
flt_cash = CASH
int_shares = 0
lst_yearly_returns = []

#Keep track of the yearly starting values
flt_account_value_start_year = flt_cash
int_days_in_year = 0
lst_trades_in_year = []

#Algorithm specific variables here
recent_low = None
recent_high = None
index_last_tx = 0
recent_low_days = 20
recent_high_days = 5
int_change_pct = 0.10 #When it moves this much
int_tx_pct = 0.50 #Buy or Sell this much of your current cash or stock

#Lets process each row, one day at a time
for indexToday in range(0, len(lst_daily_data)):
    row_day = lst_daily_data[indexToday]

    ################### ALGORITHM - START #####################

    #Determine the trade for today
    #Keep track of recent min and max values
    #If price dips too much since the recent high, buy some
    #If price goes up too much since recent low, sell some

    #Initialize for first day, and buy in half
    if (indexToday == 0):
        recent_low = {"PRICE": row_day['CLOSE_PRICE'], "DATE": row_day['DAY']}
        recent_high = {"PRICE": row_day['CLOSE_PRICE'], "DATE": row_day['DAY']}
        index_last_tx = 0
        flt_amount_to_buy = flt_cash * 0.50
        (int_shares_bought, flt_cash_left) = utility.buy(flt_amount_to_buy, row_day['CLOSE_PRICE'])
        int_shares = int_shares + int_shares_bought
        flt_cash = flt_cash - flt_amount_to_buy + flt_cash_left
        the_trade = utility.log_tx('BUY_INIT', int_shares_bought, row_day)
        lst_trades_in_year.append(the_trade)
    #Check is there has been a big enough drop, so should we buy?
    elif (row_day['CLOSE_PRICE'] < recent_high["PRICE"] * (1-int_change_pct)):
        index_last_tx = indexToday
        flt_amount_to_buy = flt_cash * int_tx_pct
        (int_shares_bought, flt_cash_left) = utility.buy(flt_amount_to_buy, row_day['CLOSE_PRICE'])
        int_shares = int_shares + int_shares_bought
        flt_cash = flt_cash - flt_amount_to_buy + flt_cash_left
        the_trade = utility.log_tx('BUY', int_shares_bought, row_day)
        lst_trades_in_year.append(the_trade)
    #Check is there has been a big enough rise, so should we sell?
    elif (row_day['CLOSE_PRICE'] > recent_low["PRICE"] * (1+int_change_pct)):
        index_last_tx = indexToday
        int_shares_sold = int(int_shares * int_tx_pct)
        flt_cash_profit = utility.sell(int_shares_sold, row_day['CLOSE_PRICE'])
        int_shares = int_shares - int_shares_sold
        flt_cash = flt_cash + flt_cash_profit
        the_trade = utility.log_tx('SELL', int_shares_sold, row_day)
        lst_trades_in_year.append(the_trade)
    else:
        #Dont trade today, just sit tight
        pass

    #Lets reset the recent high and low, for the next day's trading, based on today's price action
    recent_low = None
    lowRange = range(max(indexToday+1-recent_low_days, index_last_tx), indexToday+1)
    for indexLow in lowRange:
        rowLow = lst_daily_data[indexLow]
        if ((recent_low is None) or (rowLow['CLOSE_PRICE'] < recent_low["PRICE"])):
            recent_low = {"PRICE": rowLow['CLOSE_PRICE'], "DATE": rowLow['DAY']}
    rowLow = None

    recent_high = None
    highRange = range(max(indexToday+1-recent_high_days, index_last_tx), indexToday+1)
    for indexHigh in highRange:
        rowHigh = lst_daily_data[indexHigh]
        if ((recent_high is None) or (rowHigh['CLOSE_PRICE'] > recent_high["PRICE"])):
            recent_high = {"PRICE": rowHigh['CLOSE_PRICE'], "DATE": rowHigh['DAY']}
    rowHigh = None

    ################### ALGORITHM - END #######################

    #The trading day is over
    (flt_account_value_start_year, int_days_in_year, lst_trades_in_year) = utility.trading_day_closed(lst_yearly_returns,
        lst_daily_data, indexToday,
        int_shares, flt_cash, None,
        flt_account_value_start_year, int_days_in_year, lst_trades_in_year)

###########################################################

#Lets save the results
utility.write_csv_output(FILENAME_OUT, lst_yearly_returns)
