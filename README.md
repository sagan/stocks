# README #
This code is to backtest SMA50 and SMA200 for the S&P500.
And you can add any other daily values (i.e. SMA400) to the input dataset (spy.in.csv) as you dream them up.

### Algorithms ###
* spy_buy_and_hold.py = Buy, and never sell
* spy_buy_sell_200.py = Buy over SMA200, Sell under SMA200
* spy_buy_sell_400.py = Buy over SMA400, Sell under SMA400
* spy_below_200_above_50.py = Buy over SMA200, Sell under SMA200; but buy back in early using SMA50
* spy_below_400_above_200.py = Buy over either SMA200 or SMA400, Sell under SMA400
* spy_buy_on_200_dips.py = Buy over SMA200, Sell under SMA200; but buy back in early using % drops, BASED ON LAST TX PRICE
* spy_buy_on_200_dips_bear.py = Buy over SMA200, Sell under SMA200; but buy back in early using % drops, BASED LAST SELL TX PRICE (which is below the SMA200)
* spy_trailing_min_max.py = Based on last X days, if prices moves up or down Y, then make a TX.

### How to run ###
* git clone the repo
* install python, I am using 3.3.5
* If you want to do development, use PyCharm
* Or you can just run the code, "python spy_buy_and_hold.py" (or whatever algorithm file you want to run).
* Watch it go, and open the resulting CSV (with Excel)

### To make your own algorithm ###
* Copy an existing algorithm file (e.g. spy_buy_and_hold.py)
* Make your changes
* Run it
* Obviously you should spot check the TX trades
* Compare your new algorithm, year by year, to the baseline algorithm (i.e. spy_buy_and_hold.py)

### Misc ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)