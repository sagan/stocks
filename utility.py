#imports...
import math
import csv
import datetime
import baseline

#Will return both the shares, and leftover cash, from the buy order
def buy(cash, price):
    shares_exact = cash / price
    shares_whole = math.floor(shares_exact)
    cash_left_exact = cash - (shares_whole * price)
    cash_left = round(cash_left_exact, 2)
    return (shares_whole, cash_left)


#Will return the cash after the shares are sold
def sell(shares, price):
    cash_exact = shares * price
    cash = round(cash_exact, 2)
    return cash


#Determine total value of account
def total_account(shares, price, cash):
    return sell(shares, price) + cash


#Calulate the return for a given start and end
def roi(flt_start, flt_end):
    diff = flt_end - flt_start
    pct = diff / flt_start
    pct_rnd = round(100*pct, 1)
    return pct_rnd


#Lets read the input file, and put it in a Python List
def read_csv_input(s_filename, dt_start, dt_end, lst_float_columns=[]):
    lst_daily_data = []
    print("Reading File: %s" % (s_filename,))
    input_file = csv.DictReader(open(s_filename))
    for row in input_file:
        row_typed = {}
        dt_day = datetime.datetime.strptime((row['Day']), "%m/%d/%Y")

        #Only keep the days between the start and end dates of interest
        if ((dt_day >= dt_start) and (dt_day <= dt_end)):
            row_typed['DAY'] = dt_day
            row_typed['YEAR'] = dt_day.year
            row_typed['CLOSE_PRICE'] = float(row['Close'])

            #Some expected, but optional depending on the algorithm, columns
            if ('Open' in row):
                row_typed['OPEN_PRICE'] = float(row['Open'])

            if ('SMA50' in row):
                row_typed['SMA50'] = float(row['SMA50'])

            if ('SMA100' in row):
                row_typed['SMA100'] = float(row['SMA100'])

            if ('SMA200' in row):
                row_typed['SMA200'] = float(row['SMA200'])

            if ('SMA400' in row):
                row_typed['SMA400'] = float(row['SMA400'])

            if ('CloseSSO' in row):
                row_typed['SSO_PRICE'] = float(row['CloseSSO'])

            lst_daily_data.append(row_typed)
            print(row_typed)
        else:
            #We dont care about this row
            print("Skipping day: %s", (dt_day.strftime("%m/%d/%Y")))
    print("Loaded %s rows" % (len(lst_daily_data)))
    return lst_daily_data


#Lets read the input file, and put it in a Python List
def write_csv_output(s_filename, lst_yearly_returns):
    print("Writing file: %s" % (s_filename,))
    fieldnames = ['YEAR', 'ROI_BASELINE', 'ROI_PCT', 'DAYS', 'START_VALUE', 'END_VALUE', 'CASH', 'TRADES', 'TRADE_TX']
    output_file = open(s_filename, "w+")
    csvwriter = csv.DictWriter(output_file, delimiter=',', fieldnames=fieldnames)
    csvwriter.writeheader()
    for row in lst_yearly_returns:
        print(row)
        csvwriter.writerow(row)
    output_file.close()
    print("Done writing file: %s" % (s_filename,))


#Determine if year is complete, and take a snapshot
def trading_day_closed(lst_yearly_returns, lst_daily_data, indexToday,
                            flt_shares, flt_cash, flt_share_price,
                            flt_account_value_start_year, int_days_in_year, lst_trades_in_year):
    #Get today's row of data
    row = lst_daily_data[indexToday]
    int_days_in_year = int_days_in_year + 1

    #If no share price was passed in, use the default as of today's close price
    if (flt_share_price is None):
        flt_share_price = row['CLOSE_PRICE']

    #See if this is the last day of the dataset, or the year is about to change
    if ((indexToday == len(lst_daily_data) - 1) or (row['YEAR'] != lst_daily_data[indexToday+1]['DAY'].year)):
        flt_account_value_end_year = total_account(flt_shares, flt_share_price, flt_cash)
        roi_year = roi(flt_account_value_start_year, flt_account_value_end_year)

        #Capture a snapshot of the yearly results
        int_year = row['YEAR']
        lst_yearly_returns.append({'YEAR':int_year,
                                   'ROI_BASELINE': baseline.baseline_rois[int_year],
                                   'ROI_PCT':roi_year,
                                   'DAYS':int_days_in_year,
                                   'TRADES':len(lst_trades_in_year),
                                   'TRADE_TX':lst_trades_in_year,
                                   'START_VALUE':flt_account_value_start_year,
                                   'END_VALUE':flt_account_value_end_year,
                                   'CASH':flt_cash})

        #Reset results for next year processing
        int_days_in_year = 0
        lst_trades_in_year = []
        flt_account_value_start_year = flt_account_value_end_year

    return (flt_account_value_start_year, int_days_in_year, lst_trades_in_year)


def aggregate_time_period(lst_periodic_returns, lst_daily_data, current_start_year, current_end_year,
                          flt_account_value_start, flt_account_value_end, flt_cash, lst_trades_in_time_period, int_buy_dips_count):
    # Summarize the situation
    roi_total = roi(flt_account_value_start, flt_account_value_end)

    #Find baseline ROI
    startPrice = 0
    endPrice = 0
    for anIndexDay in range(0, len(lst_daily_data)):
        row_day = lst_daily_data[anIndexDay]
        if (startPrice != 0 and endPrice != 0):
            roi_baseline = roi(startPrice, endPrice)
            break
        if (startPrice == 0 and row_day['YEAR'] == current_start_year):
            startPrice = row_day['CLOSE_PRICE']
        elif (endPrice == 0 and row_day['YEAR'] == current_end_year + 1):
            row_day_yesterday = lst_daily_data[anIndexDay - 1]
            endPrice = row_day_yesterday['CLOSE_PRICE']

    # When there is not any row for the current_end_year, since it is in the future of our lst_daily_data set
    if (endPrice == 0):
        endPrice = row_day['CLOSE_PRICE']
        roi_baseline = roi(startPrice, endPrice)

    # fill out the data structure
    lst_periodic_returns.append({'START_YEAR': current_start_year,
                               'END_YEAR': current_end_year,
                               'ROI_BASELINE_PCT': roi_baseline,
                               'ROI_PCT': roi_total,
                               'TRADES': len(lst_trades_in_time_period),
                               'TRADES_DIP': int_buy_dips_count,
                               'TRADE_TX': lst_trades_in_time_period,
                               'START_VALUE': flt_account_value_start,
                               'END_VALUE': flt_account_value_end,
                               'CASH_LEFT': flt_cash})


#Lets read the input file, and put it in a Python List
def write_csv_output_for_aggregate_time_period(s_filename, lst_periodic_returns):
    print("Writing file: %s" % (s_filename,))
    fieldnames = ['START_YEAR', 'END_YEAR', 'ROI_BASELINE_PCT', 'ROI_PCT', 'START_VALUE', 'END_VALUE', 'CASH_LEFT',
                  'TRADES', 'TRADES_DIP', 'TRADE_TX']
    output_file = open(s_filename, "w+")
    csvwriter = csv.DictWriter(output_file, delimiter=',', fieldnames=fieldnames)
    csvwriter.writeheader()
    for row in lst_periodic_returns:
        print(row)
        csvwriter.writerow(row)
    output_file.close()
    print("Done writing file: %s" % (s_filename,))


#Make a standard TX object for consistency
def log_tx(buy_or_sell, int_shares, row_day):
    return (buy_or_sell, int_shares, row_day['CLOSE_PRICE'], row_day['DAY'].strftime("%m/%d/%Y"))

