#Add imports of modules here
import utility
import datetime

#Constants
FILENAME_IN = "spy.in.csv"
FILENAME_OUT = "spy_buy_and_hold.out.csv"
CASH = 10000.00
START_DATE = datetime.datetime(1950, 1, 1)
END_DATE = datetime.datetime(2100, 1, 1)

#Lets read the input file, and put it in a Python List
lst_daily_data = utility.read_csv_input(FILENAME_IN, START_DATE, END_DATE)

###########################################################

#Initialize for overall daily processing
flt_cash = CASH
int_shares = 0
lst_yearly_returns = []

#Keep track of the yearly starting values
flt_account_value_start_year = flt_cash
int_days_in_year = 0
lst_trades_in_year = []

#Algorithm specific variables here
#NONE

#Lets process each row, one day at a time
for indexToday in range(0, len(lst_daily_data)):
    row_day = lst_daily_data[indexToday]

    ################### ALGORITHM - START #####################

    #Determine the trade for today
    #We never sell, so this is an easy algorithm
    #Compare results with http://en.wikipedia.org/wiki/S%26P_500

    if (indexToday == 0):
        (int_shares, flt_cash) = utility.buy(flt_cash, row_day['OPEN_PRICE'])
        the_trade = utility.log_tx('BUY', int_shares, row_day)
        lst_trades_in_year.append(the_trade)

    ################### ALGORITHM - END #######################

    #The trading day is over
    (flt_account_value_start_year, int_days_in_year, lst_trades_in_year) = utility.trading_day_closed(lst_yearly_returns,
        lst_daily_data, indexToday,
        int_shares, flt_cash, None,
        flt_account_value_start_year, int_days_in_year, lst_trades_in_year)

###########################################################

#Lets save the results
utility.write_csv_output(FILENAME_OUT, lst_yearly_returns)
