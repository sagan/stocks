#Add imports of modules here
import utility
import datetime

#Constants
FILENAME_IN = "spy.in.csv"
FILENAME_OUT = "spy_buy_on_200_dips_bear.out.csv"
CASH = 10000.00
START_DATE = datetime.datetime(1951, 1, 1)
END_DATE = datetime.datetime(2100, 1, 1)

#Lets read the input file, and put it in a Python List
lst_daily_data = utility.read_csv_input(FILENAME_IN, START_DATE, END_DATE)

###########################################################

#Initialize for overall daily processing
flt_cash = CASH
int_shares = 0
lst_yearly_returns = []

#Keep track of the yearly starting values
flt_account_value_start_year = flt_cash
int_days_in_year = 0
lst_trades_in_year = []

#Algorithm specific variables here
fully_vested = False
int_dip_pct = 0.30 #When it goes down this much
int_buy_pct = 0.50 #Buy this much
flt_last_sell_tx_price = 0
int_number_of_dips_purchased = 0

#Lets process each row, one day at a time
for indexToday in range(0, len(lst_daily_data)):
    row_day = lst_daily_data[indexToday]
    row_yesterday = lst_daily_data[indexToday - 1]

    ################### ALGORITHM - START #####################

    #Determine the trade for today
    #When the price is above 200SMA, we get in
    #When the price is goes below 200SMA, we buy on the dips based on the price went it broke below the SMA200

    if (row_day['CLOSE_PRICE'] >= row_day['SMA200'] and not fully_vested):
        (int_shares_bought, flt_cash_left) = utility.buy(flt_cash, row_day['CLOSE_PRICE'])
        int_shares = int_shares + int_shares_bought
        flt_cash = flt_cash_left
        fully_vested = True
        flt_last_sell_tx_price = 0
        int_number_of_dips_purchased = 0
        the_trade = utility.log_tx('BUY', int_shares_bought, row_day)
        lst_trades_in_year.append(the_trade)
    elif (row_day['CLOSE_PRICE'] < row_day['SMA200'] and row_yesterday['CLOSE_PRICE'] >= row_yesterday['SMA200']):
        flt_cash_profit = utility.sell(int_shares, row_day['CLOSE_PRICE'])
        int_shares_sold = int_shares
        int_shares = 0
        flt_cash = flt_cash + flt_cash_profit
        fully_vested = False
        flt_last_sell_tx_price = row_day['CLOSE_PRICE']
        int_number_of_dips_purchased = 0
        the_trade = utility.log_tx('SELL', int_shares_sold, row_day)
        lst_trades_in_year.append(the_trade)
    elif (row_day['CLOSE_PRICE'] < row_day['SMA200'] and (row_day['CLOSE_PRICE'] < (1- (int_dip_pct)*(int_number_of_dips_purchased+1)) * flt_last_sell_tx_price)):
        flt_amount_to_buy = flt_cash * int_buy_pct
        (int_shares_bought, flt_cash_left) = utility.buy(flt_amount_to_buy, row_day['CLOSE_PRICE'])
        int_shares = int_shares + int_shares_bought
        flt_cash = flt_cash - flt_amount_to_buy + flt_cash_left
        fully_vested = False
        int_number_of_dips_purchased = int_number_of_dips_purchased + 1
        the_trade = utility.log_tx('BUY_DIP', int_shares_bought, row_day)
        lst_trades_in_year.append(the_trade)
    else:
        #Dont trade today, just sit tight
        pass

    ################### ALGORITHM - END #######################

    #The trading day is over
    (flt_account_value_start_year, int_days_in_year, lst_trades_in_year) = utility.trading_day_closed(lst_yearly_returns,
        lst_daily_data, indexToday,
        int_shares, flt_cash, None,
        flt_account_value_start_year, int_days_in_year, lst_trades_in_year)

###########################################################

#Lets save the results
utility.write_csv_output(FILENAME_OUT, lst_yearly_returns)
